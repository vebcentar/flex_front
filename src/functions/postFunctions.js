import axios from "axios";

let server;
if (process.env.NODE_ENV === "development") {
  //server = "http://127.0.0.10/api";
  server = "https://admin.enigmashop.me/api";

} else {
}
server = "https://admin.enigmashop.me/api";

export const authenticateUser = token => {
  return axios({
    method: "post",
    url: `${server}/auth/authenticate`,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const getAllProductsForSorting = category_id => {
  return axios({
    method: "post",
    url: `${server}/products/getAllProductsForSorting`,
    data: { category_id }
  }).then(res => res.data);
};
export const getAllCustomers = type => {
  return axios({
    method: "post",
    data: { type },
    url: `${server}/customers/getAllCustomers`
  }).then(res => res.data);
};
export const getAllUsersDiscount = id => {
  return axios({
    method: "post",
    data: { id },
    url: `${server}/discounts/getAllUsersDiscount`
  }).then(res => res.data);
};
export const getAllUserSpecificDiscount = id => {
  return axios({
    method: "post",
    data: { id },
    url: `${server}/discounts/getAllUserSpecificDiscount`
  }).then(res => res.data);
};

export const getCustomer = id => {
  return axios({
    method: "post",
    data: { id },
    url: `${server}/customers/getCustomer`
  }).then(res => res.data);
};

export const loginUser = (username, password) => {
  return axios({
    method: "post",
    url: `${server}/login`,
    data: {
      username,
      password
    }
  }).then(res => res.data);
};

export const addPage = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;
  return axios({
    method: "post",
    url: `${server}/pages/addPage`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};
export const addProduct = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;
  return axios({
    method: "post",
    url: `${server}/products/addProduct`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};

export const addImages = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;

  return axios({
    method: "post",
    url: `${server}/pages/addImages`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data );
};
export const addImagesProduct = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;
  return axios({
    method: "post",
    url: `${server}/products/addImages`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};

export const updatePage = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/updatePage`,
    data,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};
export const updateProduct = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/products/updateProduct`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const getAllPages = (language_id, limit, offset, category_id) => {

  return axios({
    method: "post",
    url: `${server}/pages/getAllPages`,
    data: {
      language_id,
      limit,
      offset,
      category_id
    }
  }).then(res => res.data);
};
export const getAllProducts = (
  limit,
  offset,
  category_id,
  name,
  discountFilter
) => {
  return axios({
    method: "post",
    url: `${server}/products/getAllProducts`,
    data: {
      limit,
      offset,
      category_id,
      name,
      discountFilter
    }
  }).then(res => res.data);
};
export const getAllDiscountedProducts = (user_id, discount_id) => {
  return axios({
    method: "post",
    url: `${server}/products/getAllDiscountedProducts`,
    data: {
      user_id,
      discount_id
    }
  }).then(res => res.data);
};

export const getPage = slug => {
  return axios({
    method: "post",
    url: `${server}/pages/getPage`,
    data: {
      slug,
      admin: true
    }
  }).then(res => res.data);
};
export const getProduct = id => {
  return axios({
    method: "post",
    url: `${server}/products/getProduct`,
    data: {
      id,
      admin: true
    }
  }).then(res => res.data);
};

export const deletePage = (page_id, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/deletePage`,
    data: {
      page_id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteUserDiscount = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/discounts/deleteUserDiscount`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteProduct = (product_id, token) => {
  return axios({
    method: "post",
    url: `${server}/products/deleteProduct`,
    data: {
      product_id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const toggleSpecialOffer = (id, offer, token) => {
  return axios({
    method: "post",
    url: `${server}/products/toggleSpecialOffer`,
    data: {
      id,
      offer
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const addUserToDiscount = (user_id, discount_id, token) => {
  return axios({
    method: "post",
    url: `${server}/discounts/addUserToDiscount`,
    data: {
      user_id,
      discount_id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const setWebDiscount = (id, newState, token) => {
  return axios({
    method: "post",
    url: `${server}/discounts/setWebDiscount`,
    data: {
      id,
      newState
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteOrder = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/orders/deleteOrder`,
    data: {
      id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const deleteImage = (image, image_id, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/deleteImage`,
    data: {
      image,
      image_id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteImageProduct = (image, image_id, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/deleteImage`,
    data: {
      image,
      image_id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const sortImages = (images, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/sortImages`,
    data: {
      images
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const sortProductImages = (images, token) => {
  return axios({
    method: "post",
    url: `${server}/products/sortImages`,
    data: {
      images
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const sortMenus = (menus, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/sortMenus`,
    data: {
      menus
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const sortCategories = (categories, token) => {
  return axios({
    method: "post",
    url: `${server}/products/sortCategories`,
    data: {
      categories
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const sortProducts = (products, token) => {
  return axios({
    method: "post",
    url: `${server}/products/sortProducts`,
    data: {
      products
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const getAllTeams = () => {
  return axios({
    method: "post",
    url: `${server}/teams/getAllTeams`
  }).then(res => res.data);
};
export const getAllCategories = () => {
  return axios({
    method: "post",
    url: `${server}/players/getAllCategories`
  }).then(res => res.data);
};
export const getAllCategoriesPage = () => {
  return axios({
    method: "post",
    url: `${server}/pages/allCategories`
  }).then(res => res.data);
};
export const getAllVideos = () => {
  return axios({
    method: "post",
    url: `${server}/videos/getAllVideos`
  }).then(res => res.data);
};
export const getAllDiscounts = () => {
  return axios({
    method: "post",
    url: `${server}/discounts/getAllDiscounts`
  }).then(res => res.data);
};
export const getUserSpecificDiscount = id => {
  return axios({
    method: "post",
    data: { id },
    url: `${server}/discounts/getUserSpecificDiscount`
  }).then(res => res.data);
};

export const getSpecificDiscounts = id => {
  return axios({
    method: "post",
    data: { id },
    url: `${server}/discounts/getSpecificDiscounts`
  }).then(res => res.data);
};
export const getAllSpecificDiscounts = () => {
  return axios({
    method: "post",
    url: `${server}/discounts/getAllSpecificDiscounts`
  }).then(res => res.data);
};
export const handleSpecificDiscount = (data, token) => {
  return axios({
    method: "post",
    data,
    headers: {
      Authorization: token
    },
    url: `${server}/discounts/handleSpecificDiscount`
  }).then(res => res.data);
};

export const getAllCompetitions = () => {
  return axios({
    method: "post",
    url: `${server}/competitions/getAllCompetitions`
  }).then(res => res.data);
};

export const getAllMenus = () => {
  return axios({
    method: "post",
    url: `${server}/menus/getAllMenus`
  }).then(res => res.data);
};

export const addTeam = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/teams/addTeam`,
    data,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};

export const addVideo = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/videos/addVideo`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const handleDiscount = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/customers/handleDiscount`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const addDiscount = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/discounts/addDiscount`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const addCompetition = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/competitions/addCompetition`,
    data,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};
export const addMenuItem = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/addMenuItem`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const addProductCategory = (data, token) => {

  return axios({
    method: "post",
    url: `${server}/products/addProductCategory`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res =>  res.data );
};
export const editMenuItem = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/editMenuItem`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const editProductCategory = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/products/editProductCategory`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const editCompetition = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/competitions/editCompetition`,
    data,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};

export const editTeam = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/teams/editTeam`,
    data,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};
export const editVideo = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/videos/editVideo`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const editDiscount = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/discounts/editDiscount`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const addGame = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/games/addGame`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const editGame = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/games/editGame`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const getAllGames = () => {
  return axios({
    method: "post",
    url: `${server}/games/getAllGames`
  }).then(res => res.data);
};
export const getGame = id => {
  return axios({
    method: "post",
    url: `${server}/games/getGame`,
    data: {
      id
    }
  }).then(res => res.data);
};

export const deleteTeam = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/teams/deleteTeam`,
    data: {
      id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteMenuItem = (id, menu_id, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/deleteMenuItem`,
    data: {
      id,
      menu_id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteProductCategory = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/products/deleteProductCategory`,
    data: {
      id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const deleteVideo = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/videos/deleteVideo`,
    data: {
      id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteDiscount = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/discounts/deleteDiscount`,
    data: {
      id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteMenu = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/deleteMenu`,
    data: {
      id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteGame = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/games/deleteGame`,
    data: {
      id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const addPlayer = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;
  return axios({
    method: "post",
    url: `${server}/players/addPlayer`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};

export const addPlayerCategory = (name, token) => {
  return axios({
    method: "post",
    url: `${server}/players/addCategory`,
    data: { name },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const addMenu = (name, position, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/addMenu`,
    data: { name, position },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const editMenu = (name, position, id, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/editMenu`,
    data: { name, position, id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const addPageCategory = (name, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/addCategory`,
    data: { name },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deletePlayerCategory = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/players/deleteCategory`,
    data: { id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const deletePageCategory = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/deleteCategory`,
    data: { id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const editPlayerCategory = (id, name, token) => {
  return axios({
    method: "post",
    url: `${server}/players/editCategory`,
    data: { id, name },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const editPageCategory = (id, name, token) => {
  return axios({
    method: "post",
    url: `${server}/pages/editCategory`,
    data: { id, name },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const addStaff = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;
  return axios({
    method: "post",
    url: `${server}/staff/addStaff`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};
export const updateStaff = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;
  return axios({
    method: "post",
    url: `${server}/staff/updateStaff`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};

export const updatePlayer = (data, token, config) => {
  const onUploadProgress = config.onUploadProgress;
  return axios({
    method: "post",
    url: `${server}/players/updatePlayer`,
    data,
    onUploadProgress,
    headers: {
      "content-type": "multipart/form-data",
      Authorization: token
    }
  }).then(res => res.data);
};

export const getAllPlayers = category => {
  return axios({
    method: "post",
    data: { category },
    url: `${server}/players/getAllPlayers`
  }).then(res => res.data);
};
export const getAllStaff = () => {
  return axios({
    method: "post",
    url: `${server}/staff/getAllStaff`
  }).then(res => res.data);
};

export const getPlayer = id => {
  return axios({
    method: "post",
    url: `${server}/players/getPlayer`,
    data: { id }
  }).then(res => res.data);
};
export const getAllMenuItems = id => {
  return axios({
    method: "post",
    url: `${server}/menus/getAllMenuItems`,
    data: { id }
  }).then(res => res.data);
};
export const getAllProductCategory = () => {
  return axios({
    method: "post",
    url: `${server}/products/getAllProductCategory`
  }).then(res => res.data );
};
export const getAllSizes = () => {
  return axios({
    method: "post",
    url: `${server}/products/getAllSizes`
  }).then(res => res.data);
};
export const getAllOrders = () => {
  return axios({
    method: "post",
    url: `${server}/orders/getAllOrders`
  }).then(res => res.data);
};
export const getOrder = id => {
  return axios({
    method: "post",
    data: { id },
    url: `${server}/orders/getOrder`
  }).then(res => res.data);
};

export const getAllParentMenus = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/menus/getAllParentMenus`,
    data: { id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const getStaff = id => {
  return axios({
    method: "post",
    url: `${server}/staff/getSingleStaff`,
    data: { id }
  }).then(res => res.data);
};
export const getTable = competition_id => {
  return axios({
    method: "post",
    url: `${server}/tables/getTable`,
    data: { competition_id }
  }).then(res => res.data);
};

export const deletePlayer = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/players/deletePlayer`,
    data: { id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteStaff = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/staff/deleteStaff`,
    data: { id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const deleteFromTable = (id, competition_id, token) => {
  return axios({
    method: "post",
    url: `${server}/tables/deleteFromTable`,
    data: { id, competition_id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
export const updateTable = (data, token) => {
  return axios({
    method: "post",
    url: `${server}/tables/updateTable`,
    data,
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const deleteCompetition = (id, token) => {
  return axios({
    method: "post",
    url: `${server}/competitions/deleteCompetition`,
    data: { id },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};

export const addTeamToTables = (team_id, competition_id, token) => {
  return axios({
    method: "post",
    url: `${server}/tables/addTeam`,
    data: {
      team_id,
      competition_id
    },
    headers: {
      Authorization: token
    }
  }).then(res => res.data);
};
