import React from "react";

const Logo = props => {
  return <img src="/logo.png" alt="logo"></img>;
};

export default Logo;
