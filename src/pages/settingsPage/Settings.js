import React, { Component } from "react";
import { LinkButton } from "../../components/Buttons/Button";

class SettingsPage extends Component {
  render() {
    return (
      <div>
        <LinkButton href="dodaj-stranicu">Dodaj stranicu</LinkButton>
      </div>
    );
  }
}

export default SettingsPage;
