import React, { Component } from "react";
import {
  CustomInput,
  CustomTextArea
} from "../../../components/Form/Inputs/Inputs";
import { createSlug } from "../../../functions/functions";
import TextEditor from "../../../components/TextEditor/TextEditor";
import { CustomButton } from "../../../components/Buttons/Button";
import { addPageValidiation } from "../../../functions/validateFunctions";
import {
  addImages,
  getAllCategoriesPage,
  getPage,
  updatePage
} from "../../../functions/postFunctions";
import SortableGrid from "../../../components/SortableGrid/SortableGrid";
import DatePicker from "react-date-picker";
import SearchableDropdown from "../../../components/Form/Dropdown/SearchableDropdown";


class EditPage extends Component {
  state = {
    pages: [
      {
        language_id: 1,
        title: "",
        description: "",
        content: "",
        slug: "",
        date: new Date(),
        content_right: ""
      }
    ],
    errors: [],
    showErrorPopup: false,
    showWarningPopup: false,
    images: null,
    active: "content",
    listOfImages: [],
    stepOne: true,
    edit: true,
    uploadedImages: [],
    completed: 0,
    showUploadPopup: false,
    uploadComplete: false,
    rightSide: false,
    categoryList: [],
    category_id: 0,
    currentCategory: { value: 0, label: "" },
    modalBtn: false,
    link: "",
    tempUrl: null,
  };


  componentDidMount() {
    this.props.setBreadcrumbs("Izmjeni stranicu");
    getPage(this.props.match.params.id).then(res => {
    console.log(res)
      if (res.success) {
        res.page.date = new Date(res.page.date);
        res.page.content_right = !res.page.content_right
          ? ""
          : res.page.content_right;
        this.setState({
          pages: [res.page],
          uploadedImages: res.images,
          rightSide: !!res.page.content_right
        });
        
      } else {
        this.props.history.push("/stranice");
      }
    });
  }
  toggleRightSideContent = () => {
    this.setState(({ rightSide }) => ({
      rightSide: !rightSide
    }));
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.errors != this.state.errors && this.state.errors.length > 0) {
      this.setState({ showErrorPopup: true });
    }
    if (prevState.stepOne !== this.state.stepOne) {
      const item = document.querySelector(".gallery");
      this.navigationMove(item, "gallery");
    }
    if (prevState.pages !== this.state.pages && this.state.pages.length > 0) {
      getAllCategoriesPage().then(res => {
        if (res.success) {
          const categoryList = res.categories.map(e => ({
            value: e.id,
            label: e.name
          }));
          const currentCategory = categoryList.find(
            e => e.value === this.state.pages[0].category_id
          );
          this.setState({ categoryList, currentCategory });
        }
      });
    }
  }

  cancelChanges = () => {
    this.props.history.push("/");
  };

  handleInput = (name, value, index) => {
    if (name === "currentCategory") {
      this.setState({ [name]: value });
    } else if (name === "title") {
      this.setState(({ pages }) => ({
        pages: [
          ...pages.slice(0, index),
          {
            ...pages[index],
            title: value,
            slug: createSlug(value)
          },
          ...pages.slice(index + 1)
        ]
      }));
    } else {
      this.setState(({ pages }) => ({
        pages: [
          ...pages.slice(0, index),
          {
            ...pages[index],
            [name]: value
          },
          ...pages.slice(index + 1)
        ]
      }));
    }
  };

  handleFileInput = event => {
    this.setState({ images: [...event] });
    this.setState({ tempUrl: URL.createObjectURL(event[0]) });
  };

  handleLinkInput = ( link, value) => {
    this.setState({ [link]: value });
  };

  removePopup = () => {
    this.props.history.push("/");
  };

  handleSubmit = event => {
    event.preventDefault();
    const { active } = this.state;
    const errors = addPageValidiation(this.state, true);
    if (errors.length === 0) {
      if (active === "content") {
        this.editPage();
      } else {
        this.handleAddImages();
      }
    } else {
      this.setState({ errors });
    }
  };

  editPage = () => {
    const data = new FormData();
    const pages = this.state.pages.map(e => {
      if (!this.state.rightSide) {
        e.content_right = null;
      }
      e.category_id = this.state.currentCategory.value;
      return e;
    });
    data.append("pages", JSON.stringify(pages));
    updatePage(data, this.props.token).then(res => {
      if (res.success) {
        this.setState({ showWarningPopup: true });
      } else {
        this.setState({ showErrorPopup: true });
      }
    });
  };

  handleAddImages = () => {
    const { images, pages, link } = this.state;
    const data = new FormData();

    if (images.length > 0) {
      images.forEach(e => {
        data.append("images[]", e);
      });
    }
    data.append('link', link);
    data.append("page_id", pages[0].page_id);

    const config = {
      onUploadProgress: progressEvent => {
        const completed = Math.round(
          (progressEvent.loaded * 100) / progressEvent.total
        );
        this.setState({
          completed,
          uploadComplete: completed === 100
        });
      }
    };
    addImages(data, this.props.token, config).then(res => {
      if (res.success) {
        this.props.history.push("/");
      } else {
        this.setState({ showErrorPopup: true });
      }
    });
  };

  removeImage = id => {
    const { uploadedImages } = this.state;

    const newList = uploadedImages.filter(e => e.id !== id);
    this.setState({ uploadedImages: newList });

  };

  showBtnModal = () => {
    if(this.state.modal){
      this.state.modal = !this.state.modal
    }else{
      this.state.modal = this.state.modal
    }
  }

  underlineRef = React.createRef();

  navigationMove = (item, active) => {
    item.parentElement.querySelectorAll(".link").forEach(e => {
      e.classList.remove("text-color-primary");
      e.classList.remove("text-color-font-l");
    });
    item.classList.add("text-color-primary");
    this.underlineRef.current.style.transform = `translateX(${item.offsetLeft}px)`;
    this.setState({ active });
  };

  render() {
    const { categoryList, currentCategory } = this.state;
    
    return (
      <div className="wrapper">
        <div
          className={`errorPopup ${
            this.state.showUploadPopup ? "popupShown" : ""
          }`}
        >
          <div className="content py-20 px-10 d-flex justify-content-center flex-column">
            <h3 className="f-s-22 f-w-4 uppercase text-center text-color-primary">
              {!this.state.uploadComplete
                ? "Upload u toku"
                : "Optimizacija u toku"}
            </h3>
            {!this.state.uploadComplete ? (
              <div className="uploadBar d-flex align-items-center p-r">
                <div className="barWrapper">
                  <span
                    className="bar d-flex align-items-center justify-content-center"
                    style={{ width: `${this.state.completed}%` }}
                  />
                </div>
                <span className="outerProgress f-s-16 center-a-b text-color-primary">
                  {this.state.completed}%
                </span>
              </div>
            ) : (
              <div className="optimization loader d-flex justify-content-center align-items-center">
                <div className="fancy-spinner">
                  <div className="ring" />
                  <div className="ring" />
                  <div className="dot" />
                </div>
              </div>
            )}
          </div>
        </div>
        <div
          className={` errorPopup ${
            this.state.showWarningPopup ? "popupShown" : ""
          }`}
        >
          <div className="content py-20 px-10 d-flex justify-content-between flex-column">
            <h3 className="f-s-22 f-w-4 uppercase text-center text-color-primary">
              Uspjeh
            </h3>
            <h5 className="f-s-16 f-w-4 uppercase text-center">
              Stranica uspješno ažurirana
            </h5>
            <div className="button text-center mt-30">
              <CustomButton onClick={this.removePopup}>U redu</CustomButton>
            </div>
          </div>
        </div>
        {this.state.edit ? (
          <div className="innerNavigation pb-10 mb-40">
            <span
              className="f-s-16 mr-20 uppercase text-color-primary link"
              onClick={e => this.navigationMove(e.currentTarget, "content")}
            >
              Sadržaj
            </span>
            <span
              className="f-s-16 uppercase text-color-font-l link gallery"
              onClick={e => this.navigationMove(e.currentTarget, "gallery")}
            >
              Galerija
            </span>
            <span className="indicator" ref={this.underlineRef} />
          </div>
        ) : (
          ""
        )}
        {this.state.active === "content" ? (
          <div className="contentWrapper">
            <div
              className={` errorPopup ${
                this.state.showErrorPopup ? "popupShown" : ""
              }`}
            >
              <div className="content py-20 px-10 d-flex justify-content-between flex-column">
                <h3 className="f-s-18 f-w-4 uppercase text-center">
                  Problemi prilikom kreiranja ažuriranja
                </h3>
                <ul className="mt-30">
                  {this.state.errors.map((e, index) => {
                    return (
                      <li key={index} className="f-s-16 text-center">
                        {e}
                      </li>
                    );
                  })}
                </ul>
                <div className="button text-center mt-30">
                  <CustomButton onClick={this.removePopup}>
                    Zatvori
                  </CustomButton>
                </div>
              </div>
            </div>

            <form onSubmit={e => e.preventDefault()}>
              <h2 className="uppercase f-s-20 text-color-primary f-w-4">
                Crnogorski
              </h2>
              <div className="row mt-20">
                <div className="col-lg-4">
                  <CustomInput
                    label="Naslov"
                    value={this.state.pages[0].title}
                    handleChange={this.handleInput}
                    name="title"
                    index={0}
                  />
                </div>
                <div className="col-lg-3">
                  <DatePicker
                    onChange={e => this.handleInput("date", e, 0)}
                    value={this.state.pages[0].date}
                    locale={"bs-BS"}
                  />
                </div>
                <div className="col-lg-3">
                  <CustomInput
                    label="Link"
                    value={this.state.pages[0].slug}
                    handleChange={this.handleInput}
                    name="slug"
                    readOnly
                  />
                </div>
                <div className="col-md-2">
                  <SearchableDropdown
                    data={categoryList}
                    placeholder="Kategorija"
                    name="currentCategory"
                    handleChange={this.handleInput}
                    value={currentCategory}
                  />
                </div>
              </div>
              <div className="row mt-30">
                <div className="col-12">
                  <CustomTextArea
                    label="Opis"
                    value={this.state.pages[0].description}
                    handleChange={this.handleInput}
                    name="description"
                    className="textArea"
                    index={0}
                  />
                </div>
              </div>
              <div className="row mt-30">
                <div className="col-12 text-right">
                  <CustomButton onClick={this.toggleRightSideContent}>
                    {!this.state.rightSide
                      ? "Dodaj drugu kolonu"
                      : "Ukloni drugu kolonu"}
                  </CustomButton>
                </div>
              </div>
              <div className="row mt-10">
                <div
                  className={`${!this.state.rightSide ? "col-12" : "col-md-6"}`}
                >
                  <TextEditor
                    name="content"
                    handleInput={this.handleInput}
                    index={0}
                    value={this.state.pages[0].content}
                  />
                </div>
                {this.state.rightSide && (
                  <div className="col-md-6">
                    <TextEditor
                      name="content_right"
                      handleInput={this.handleInput}
                      index={0}
                      value={this.state.pages[0].content_right}
                    />
                  </div>
                )}
              </div>
            </form>
          </div>
        ) : (
          <div>
          <CustomButton 
            onClick={(e) => {this.setState(prevState => ({modalBtn: !prevState.modalBtn}));}}
            >
            Dodaj fotografiju
            </CustomButton>
            
          <div
            className={`addTeamModal d-flex justify-content-center align-items-center ${
              this.state.modalBtn ? "visible" : ""
            }`} style={{overflowY: 'auto'}}
          >
              <div className="addTeamContent py-30 px-30">
                <h4 className="text-color-primary f-s-20 lh text-center mb-20">
                Dodaj Sliku
                </h4>

                <div className="col align-self-center ">

                  
                  <label
                    htmlFor="name"
                    className="f-s-14 text-color-font-l"
                    style={{
                      color: "#069447",
                      marginBottom: "5px",
                      display: "block"
                    }}
                  >
                    Link
                  </label>
                  <CustomInput
                    value={this.state.link}
                    handleChange={this.handleLinkInput}
                    name="link"
                    index={0}
                    counter={false}
                  />
                

                <br></br>
                <br></br>

                    <CustomInput
                      label="Izaberi fotografiju"
                      value={null}
                      handleChange={this.handleFileInput}
                      name="file[]"
                      index={0}
                      type="file"
                      multiple
                    />
                  
                </div>

 
                {this.state.images ? (
                  <div className="selectedImages my-20 py-10 px-15">
                    <h3 className="f-s-16 text-color-primary f-w-4">
                      Fotografije odabrane za upload:
                    </h3>
                    <ul className="mt-20">
                      {this.state.images.map((e, index) => {
                        return (
                          <li key={index} className="f-s-12">
                            {index + 1}. {e.name}
                            <div style={{width: '50%', height: '50%'}}>
                              <img src={this.state.tempUrl} alt="product category" style={{width: '100%', height: '100%'}}/>
                            </div>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                ) : (
                  ""
                )}

                <div className="row mt-40">
                  <div className="col-lg-12 d-flex justify-content-center">
                    <CustomButton
                      className="mr-30"
                      onClick={e => this.handleSubmit(e)}
                    >
                      Dodaj 
                    </CustomButton>
                    <CustomButton className="red" onClick={(e) => {this.setState(prevState => ({modalBtn: !prevState.modalBtn}));}}>
                      Odustani
                    </CustomButton>
                  </div>
                </div>

            </div>
          </div>
            {
            /* 
          <div className="galleryWrapper">
            <CustomInput
              label="Izaberi fotografije"
              value={null}
              handleChange={this.handleFileInput}
              name="file[]"
              index={0}
              type="file"
              multiple
            />
            {this.state.images ? (
              <div className="selectedImages my-20 py-10 px-15">
                <h3 className="f-s-16 text-color-primary f-w-4">
                  Fotografije odabrane za upload:
                </h3>
                <ul className="mt-20">
                  {this.state.images.map((e, index) => {
                    return (
                      <li key={index} className="f-s-12">
                        {index + 1}. {e.name}
                      </li>
                    );
                  })}
                </ul>
              </div>
            ) : (
              ""
            )}
            <SortableGrid
              images={this.state.uploadedImages}
              edit
              token={this.props.token}
              removeImage={this.removeImage}
            />
          </div>*/}
            <div style={{marginTop: '50px'}}>
              <SortableGrid
                images={this.state.uploadedImages}
                edit
                token={this.props.token}
                removeImage={this.removeImage}
              />
            </div>
            
          </div> 
          
        )}
        <div className="row mt-30">
          <div className="col-12 d-flex justify-content-end">
            <CustomButton onClick={e => this.handleSubmit(e)} className="mr-20">
              {this.state.active === "content"
                ? "Ažuriraj stranicu"
                : "Dodaj fotografije"}
            </CustomButton>
            <CustomButton className="red" onClick={this.cancelChanges}>
              Odustani od izmjena
            </CustomButton>
          </div>
        </div>
      </div>
    );
  }
}

export default EditPage;
