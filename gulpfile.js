const gulp = require("gulp");
const concat = require("gulp-concat");
const shell = require("gulp-shell");

gulp.task("compile", shell.task("yarn build"));

gulp.task("copyFiles", function() {
  return gulp.src("build/**/*.*").pipe(gulp.dest("../api/public"));
});

gulp.task("copyHTML", function() {
  return gulp
    .src("build/index.html")
    .pipe(concat("welcome.blade.php"))
    .pipe(gulp.dest("../api/resources/views"));
});

gulp.task("build", gulp.series("compile", "copyFiles", "copyHTML"));
